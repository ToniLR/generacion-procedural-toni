﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorPerlin : MonoBehaviour
{
    [System.Serializable]
    public class SplatHeights
    {
        public int textureIndex;
        public int startingHeight;


    }
    public SplatHeights[] splatheights;

    public int amplitud = 20;

    public int ampladax = 256;
    public int llargadaz = 256;
    public float freq = 20;
   
    public float seed = 2313223f;
    
    public void Start()
    {
        Terrain terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerarTerrain(terrain.terrainData);
        
    }

    public TerrainData GenerarTerrain(TerrainData terrainData)
    {
        terrainData.heightmapResolution = ampladax + 1;
        terrainData.size = new Vector3(ampladax, amplitud, llargadaz);

        terrainData.SetHeights(0, 0, GenerarAltura());
        //terrainData = Terrain.activeTerrain.terrainData;
      
        float[,,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];
       

        for (int y = 0; y < terrainData.alphamapHeight; y++)
        {
            for (int x = 0; x < terrainData.alphamapWidth; x++)
            {
                float terrainHeight = terrainData.GetHeight(y, x);
            
                float[] splat = new float[splatheights.Length];
                for (int i = 0; i < splatheights.Length; i++)
                {
                    if (i == splatheights.Length - 1 && terrainHeight >= splatheights[i].startingHeight)
                        splat[i] = 1;

                    else if (terrainHeight >= splatheights[i].startingHeight && terrainHeight <= splatheights[i + 1].startingHeight)
                    {
                        splat[i] = 1;
                    }
                }
                for (int j = 0; j < splatheights.Length; j++)
                {
                    splatmapData[x, y, j] = splat[j];
                }
            }
        }
        terrainData.SetAlphamaps(0, 0, splatmapData);

       

        return terrainData;

    }


    private float[,] GenerarAltura()
    {
        float[,] altures = new float[ampladax, llargadaz];
        for (int x = 0; x < ampladax; x++)
        {
            for (int z = 0; z < llargadaz; z++)
            {
                altures[x, z] = Mathf.PerlinNoise(x / freq + seed, z / freq + seed);
            }
        }
        return altures;
    }
  

}
